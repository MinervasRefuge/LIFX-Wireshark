local m = LDMOD {
   -- MultiZone
   [503] = "StateZone",
   [506] = "StateMultiZone",
   [509] = "StateMultiZoneEffect",
   [512] = "StateExtendedColorZones",

   -- MultiZone [get]
   [502] = "GetColorZones",
   [507] = "GetMultiZoneEffect",
   [511] = "GetExtendedColorZones",

   -- MultiZone [set]
   [501] = "SetColorZones",
   [508] = "SetMultiZoneEffect",
   [510] = "SetExtendedColorZones"
}

local function dissector_name(tree, buffer, pinfo)
end

m.dissectorLookups = {
}

return m
