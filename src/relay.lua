local m = {}
m.packetProtoFields = {}

m.lifxPacketTypes = {
   -- Relay
   [818] = "StateRPower",

   -- Relay [get]
   [816] = "GetRPower",

   -- Relay [set]
   [817] = "SetRPower"
}

local function dissector_name(tree, buffer, pinfo)
end

m.dissectorLookups = {
}

return m
