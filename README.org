* LIFX UDP Dissector
The following repo contains a dissector for the LIFX protocol ([[https://lan.developer.lifx.com/docs/introduction][docs]]) W.I.P

** Dissector Completeness
 -  ✍ :: Written
 -   🧪  :: Tested
*** Discovery
*** Device

|  ID | Name              | ✍   | 🧪  |
|-----+-------------------+-----+-----|
|   3 | StateService      | [ ] | [ ] |
|  15 | StateHostFirmware | [ ] | [ ] |
|  17 | StateWifiInfo     | [ ] | [ ] |
|  19 | StateWifiFirmware | [ ] | [ ] |
|  22 | StatePower        | [ ] | [ ] |
|  25 | StateLabel        | [ ] | [ ] |
|  33 | StateVersion      | [x] | [x] |
|  35 | StateInfo         | [ ] | [ ] |
|  50 | StateLocation     | [ ] | [ ] |
|  53 | StateGroup        | [ ] | [ ] |
|  59 | EchoResponse      | [ ] | [ ] |
| 223 | StateUnhandled    | [ ] | [ ] |
|-----+-------------------+-----+-----|
|  14 | GetHostFirmware   | [ ] | [ ] |
|  16 | GetWifiInfo       | [ ] | [ ] |
|  18 | GetWifiFirmware   | [ ] | [ ] |
|  20 | GetPower          | [ ] | [ ] |
|  23 | GetLabel          | [ ] | [ ] |
|  32 | GetVersion        | [x] | [x] |
|  34 | GetInfo           | [ ] | [ ] |
|  48 | GetLocation       | [ ] | [ ] |
|  51 | GetGroup          | [ ] | [ ] |
|  58 | EchoRequest       | [ ] | [ ] |
|-----+-------------------+-----+-----|
|  21 | SetPower          | [ ] | [ ] |
|  24 | SetLabel          | [ ] | [ ] |
|  38 | SetReboot         | [ ] | [ ] |
|  49 | SetLocation       | [ ] | [ ] |
|  52 | SetGroup          | [ ] | [ ] |

*** Light

|  ID | Name                       | ✍   | 🧪  |
|-----+----------------------------+-----+-----|
| 107 | LightState                 | [x] | [x] |
| 108 | StateLightPower            | [ ] | [ ] |
| 121 | StateInfrared              | [ ] | [ ] |
| 144 | StateHevCycle              | [ ] | [ ] |
| 147 | StateHevCycleConfiguration | [ ] | [ ] |
| 149 | StateLastHevCycleResult    | [ ] | [ ] |
|-----+----------------------------+-----+-----|
| 101 | GetColour                  | [ ] | [ ] |
| 116 | GetLightPower              | [ ] | [ ] |
| 120 | GetInfrared                | [ ] | [ ] |
| 142 | GetHevCycle                | [ ] | [ ] |
| 145 | GetHevCycleConfiguration   | [ ] | [ ] |
| 148 | GetLastHevCycleResult      | [ ] | [ ] |
|-----+----------------------------+-----+-----|
| 102 | SetColour                  | [x] | [x] |
| 103 | SetWaveform                | [ ] | [ ] |
| 117 | SetLightPower              | [ ] | [ ] |
| 119 | SetWaveformOptional        | [x] | [x] |
| 122 | SetInfrared                | [ ] | [ ] |
| 143 | SetHevCycle                | [ ] | [ ] |
| 146 | SetHevCycleConfiguration   | [ ] | [ ] |

*** MultiZone

|  ID | Name                    | ✍   | 🧪  |
|-----+-------------------------+-----+-----|
| 503 | StateZone               | [ ] | [ ] |
| 506 | StateMultiZone          | [ ] | [ ] |
| 509 | StateMultiZoneEffect    | [ ] | [ ] |
| 512 | StateExtendedColorZones | [ ] | [ ] |
|-----+-------------------------+-----+-----|
| 502 | GetColorZones           | [ ] | [ ] |
| 507 | GetMultiZoneEffect      | [ ] | [ ] |
| 511 | GetExtendedColorZones   | [ ] | [ ] |
|-----+-------------------------+-----+-----|
| 501 | SetColorZones           | [ ] | [ ] |
| 508 | SetMultiZoneEffect      | [ ] | [ ] |
| 510 | SetExtendedColorZones   | [ ] | [ ] |

*** Relay

|  ID | Name        | ✍   | 🧪  |
|-----+-------------+-----+-----|
| 818 | StateRPower | [ ] | [ ] |
|-----+-------------+-----+-----|
| 816 | GetRPower   | [ ] | [ ] |
|-----+-------------+-----+-----|
| 817 | SetRPower   | [ ] | [ ] |

*** Tile

|  ID | Name             | ✍   | 🧪  |
|-----+------------------+-----+-----|
| 702 | StateDeviceChain | [ ] | [ ] |
| 711 | State64          | [ ] | [ ] |
| 720 | StateTileEffect  | [ ] | [ ] |
|-----+------------------+-----+-----|
| 701 | GetDeviceChain   | [ ] | [ ] |
| 707 | Get64            | [ ] | [ ] |
| 718 | GetTileEffect    | [ ] | [ ] |
|-----+------------------+-----+-----|
| 703 | SetUserPosition  | [ ] | [ ] |
| 715 | Set64            | [ ] | [ ] |
| 719 | SetTileEffect    | [ ] | [ ] |

*** Other

