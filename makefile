.PHONY: clean

build/lifx.lua: $(wildcard src/*.lisp) $(wildcard src/*.guile)
	rm build/lifx.lua -f
	guile --no-auto-compile src/0-natives.guile > build/lifx.lisp
	cat src/*.lisp >> build/lifx.lisp
	urn build/lifx.lisp --emit-lua build/lifx.lua

build: build/lifx.lua

clean:
	rm -f build/*

run: build/lifx.lua
	lua build/lifx.lua
